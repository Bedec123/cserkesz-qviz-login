const app = require('express')();
const jwt = require('jsonwebtoken')
const cookieParser = require('cookie-parser');
const helpers = require('handlebars-helpers');
const ehandlebars = require('express-handlebars');

const STATIC_DIR = `${__dirname}/static`;
const PORT = 8000
const SECRET = "bb993e4a166ef6113d91042020cd49a43c0a45dcaf64490fc60b96262e0ba2b6";

class Question{
    constructor(text,answers) {
        this.text = text;
        this.answers = answers; 
    }

    isCorrect(answers) {
        return JSON.stringify(this.answers) == JSON.stringify(answers);
    }

    getText() {
        return this.text;
    }
}

const QUESTIONS = {
    1: {
        questions: [
            new Question(['Rendezd időrendi sorrendbe az alábbi eseményhez tartozó dátumokat',
            '~ Házasságot kötött Olave St. Clair Soamesszal ~', 
            '~ Cserkészet fiúknak című könyv megjelenése ~',
            '~ BI-PI nyugdíjba vonult ~'
            ],['1908','1910','1912']),
            
            new Question(['Rendezd időrendi sorrendbe az alábbi eseményhez tartozó dátumokat:',
            '~ Házasságot kötött Olave St. Clair Soamesszal ~',
            '~ Brownsea szigetén megszerveződött az első cserkész tábor ~',
            '~ Útjára indult a lánycserkész mozgalom ~'
            ],['1907','1910','1912']),
            
            new Question(['Rendezd időrendi sorrendbe az alábbi eseményhez tartozó dátumokat:',
            '~ BI-PI kihírdette Birkenheadben (a YMCA-székházában) a cserkészet megalakulását ~',
            '~ Az első cserkész világtalálkozó (dzsembori) ~' ,
            '~ BI-PI meghalt ~'
            ],['1908','1920','1941'])
        ],
        banTime : 3 // in minute
        },
    2: {
        questions: [
            new Question(['Hogyan kezdődik az a levél amelyből alább idézve van',
            `\"Nagyon boldog életem volt és azt akarom, hogy a tiétek is az legyen.
            Hiszem, hogy Isten azért teremtett bennünket ebbe a szép világba, hogy boldogok legyünk és élvezzük az életet.\"`
            ],'Kedves cserkészek!'),
            new Question(['Hogyan végződik az a levél amelyből alább idzéve van',
            `Nagyon boldog életem volt és azt akarom, hogy a tiétek is az legyen.
            Hiszem, hogy Isten azért teremtett bennünket ebbe a szép világba, hogy boldogok legyünk és élvezzük az életet.`
            ],"Isten segítsen ebben benneteket."),
        ],
        banTime : 0.2, // in minute
        },
    3: {
        questions: [
            new Question(["Mi a Románia Magyar Cserkészövetség elnökének a telefonszámának az utolsó számjegye?"],'555'),
            new Question(["Mi a Románia Magyar Cserkészövetség ügyvezető elnökének a telefonszámának az utolsó számjegye?"],'780'),
            new Question(["Mi a Románia Magyar Cserkészövetség mozhalmi vezetőjének a telefonszámának az utolsó számjegye?"],'403'),
           ],
        banTime : 7, // in minute
        }
};


app.use(cookieParser());

app.set('view engine', 'hbs');
app.set('views',`${__dirname}/views`);
app.engine('hbs', ehandlebars({
    extname: 'hbs',
    helpers: helpers(),
  }));
  
// ----- STATIC SERVE ----------------------------------------------------
app.use(require('express').static(STATIC_DIR));

// middleware to auth


function generateRandomNumber(a,b) {
    return randomNum = Math.floor((Math.random() * b) + a);
}


app.use((req,resp,next) => {
    req.localStorage = {};
    
    if(!req.cookies['bb-ov-belepes']) {
        const maximNum =  QUESTIONS[1].questions.length-1;
        const minimNum = 0;
        const randomNum = generateRandomNumber(minimNum,maximNum);
        const token = jwt.sign({
            ban:false,
            banTime : undefined,
            question:1,
            nr: randomNum
        },SECRET);
        const cookieOptions = {
            maxAge: 1000 * 60 * 60 * 5, // 0.5 h
            httpOnly: true,
        };

        resp.cookie('bb-ov-belepes',token,cookieOptions);

        req.localStorage.question = 1;
        req.localStorage.nr = randomNum;
        
        next();

    } else {
        const token = req.cookies['bb-ov-belepes'];
        console.log(token);
        jwt.verify(token,SECRET,(err, payload) => {
            req.localStorage = payload;
            console.log(JSON.stringify(payload));
            next();
        });
    }
});

// bann check
app.use((req, resp, next) => {

    let err = undefined;
    if (req.localStorage.ban == true) {
        
        if ( (Date.now() - req.localStorage.banTime) >= 
            QUESTIONS[req.localStorage.question].banTime *1000*60 ) {
            const token = jwt.sign({
                ban:false,
                banTime: undefined,
                question:req.localStorage.question,
                nr: req.localStorage.nr
            },SECRET);
            const cookieOptions = {
                maxAge: 1000 * 60 * 60 * 5, // 0.5 h
                httpOnly: true,
            };
            resp.cookie('bb-ov-belepes',token,cookieOptions);
        } else {
            const bant = QUESTIONS[req.localStorage.question].banTime *60;
            let wTime = bant - (Date.now() - req.localStorage.banTime) /1000; 
            wTime = Math.floor(wTime);
            err = {message:`Helytelen válasz miatt várnod kell ${ (wTime <= 60)? wTime : Math.floor(wTime/60) } ${(wTime <= 60) ? 'masodpercet':'percet'}`};
        }        
    } 

    const actQuestion = QUESTIONS[req.localStorage.question].questions[req.localStorage.nr];
    req.localStorage.questionMessage = {
        error: err,
        message: actQuestion.getText(),
        type: req.localStorage.question
    };

    next();
});

app.get('/answer', (req, resp) => {
    // check if the answer is ok
    // if is ok rewrite jwt token and let the next question
    const ansNum = req.localStorage.question;
    const question = QUESTIONS[ansNum];
    const query = req.query;

    let ansIsCorrect = question.questions[req.localStorage.nr].isCorrect(query.data);
    
    console.log(`The answer is correct : ${ansIsCorrect}`);
    
    if (ansIsCorrect) {
        const newQuestionNum = ansNum+1;
        if (newQuestionNum == 4) {
            resp.status(302).set('Location','https://discord.gg/gVsSAgZdgA').end();
            return;
        }
        const maximNum =  QUESTIONS[newQuestionNum].questions.length-1;
        const minimNum = 0;
        const randomNum = generateRandomNumber(minimNum,maximNum);
        const token = jwt.sign({
            ban:false,
            banTime:undefined,
            question:newQuestionNum,
            nr: randomNum
        },SECRET);
        const cookieOptions = {
            maxAge: 1000 * 60 * 60 * 5, // 0.5 h
            httpOnly: true,
        };
        resp.cookie('bb-ov-belepes',token,cookieOptions);
    } else {

        const token = jwt.sign({
            ban:true,
            banTime: Date.now(),
            question:req.localStorage.question,
            nr: req.localStorage.nr
        },SECRET);
        const cookieOptions = {
            maxAge: 1000 * 60 * 60 *5, // 0.5 h
            httpOnly: true,
        };
        resp.cookie('bb-ov-belepes',token,cookieOptions);
    }

    resp.status(302).set('Location','/').end();
});

app.get('/*', (req, resp) => {
    resp.render('questions', req.localStorage.questionMessage)
});

app.listen(PORT,() => {console.log(`App listen on port : ${PORT}`)});